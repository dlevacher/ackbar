<?php

namespace Ackbar;

session_start();
session_unset();
//session_destroy();
//session_start();
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/Ackbar.php';

$ackbar = new Ackbar();
if ($ackbar->getInterfaceType() == 'cli') $ackbar->Run();