<?php

namespace Ackbar;

session_start();

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/Ackbar.php';

$ackbar = Ackbar::getInstance();
switch ($_GET['action']) {
	case 'restart':
		$ackbar->Restart();
		break;
	case 'next':
	case 'wait':
		$ackbar->Next(1)->Run();
		break;
	case 'skip':
		$ackbar->Next(2)->Run();
		break;
	case 'start':
		$ackbar->Run();
		break;
	case 'project':
		$ackbar->Project($_GET['project'])->Run();
		break;
}