/* 
 */

$(document).ready(function () {
    $("body").delegate(".next", "click", function (e) {
        $("#page_loader").show();
        $("#shadowing").show();
        $(".button").hide();
        $("body").append($("<div>").load("./btnClick.php?action=next", null, function () {
            $("#page_loader").hide();
            $("#shadowing").hide();
        }));
    });
    $("body").delegate(".skip", "click", function (e) {
        $("#page_loader").show();
        $("#shadowing").show();
        $(".button").hide();
        $("body").append($("<div>").load("./btnClick.php?action=skip", null, function () {
            $("#page_loader").hide();
            $("#shadowing").hide();
        }));
    });

    $("body").delegate(".wait", "click", function (e) {
        $("#page_loader").show();
        $("#shadowing").show();
        $(".button").hide();
        $("body").append($("<div>").load("./btnClick.php?action=wait", null, function () {
            $("#page_loader").hide();
            $("#shadowing").hide();
        }));
    });

    $(".restart").click(function (e) {
		e.preventDefault();
        $.ajax({
            url: './btnClick.php',
            type: 'get',
            data: {'action': 'restart'},
            success: function () {
                location.reload();
            }
        });
		return false;
    });

    $(".project").click(function (e) {
		e.preventDefault();
		$(".name > h1").html($(this).html());
        $.ajax({
            url: './btnClick.php',
            type: 'get',
            data: {'action': 'project', 'project': $(this).data('projectid')},
            success: function (res) {
                $('.allSteps').html(res);
            }
        });
		return false;
    });

	function start() {	
		//First launch
		$("#page_loader").show();
		$("#shadowing").show();
		$.ajax({
			type: 'get',
			url: './btnClick.php',
			data: {'action': 'start'}
		}).done(function (res) {
			$('.allSteps').html(res);
		}).always(function () {
			$("#page_loader").hide();
			$("#shadowing").hide();
		});
	}

	if ($(".project").length == 0) {
		start();
	}
	
});

//Function composerUpdate
function composerUpdate(action) {
	$('#modal_ComposerUpdate').foundation('reveal', action);
}