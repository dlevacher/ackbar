<?php

namespace Ackbar;

// require_once __DIR__ . '/vendor/autoload.php';

/**
 * Description of AckbarConfiguration
 *
 * @author Damien Levacher https://github.com/dlevacher
 */

use Symfony\Component\Yaml\Parser;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Configuration {
	CONST FILENAME = "ackbar.yml";

	protected $yamlParser = null; //Object Parser
	protected $configYml = null; //Content data of configuration.yml
	protected $options = array(); // List of options
	protected $projects = array(); // List of projects

	public function __construct() {
		if (!file_exists($filepath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . self::FILENAME))
			if (!file_exists($filepath = getcwd() . DIRECTORY_SEPARATOR . self::FILENAME))
				if (!file_exists($filepath = getcwd() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . self::FILENAME))
					throw new \Exception("No configuration file found in '{$filepath}'");
		try {
			$this->yamlParser = new Parser();
			$this->configYml = $this->yamlParser->parse(file_get_contents($filepath));
		} catch (ParseException $e) {
		    throw new \Exception("Unable to parse the YAML string: %s", $e->getMessage());
		}
	}
	
	/**
	 * Return projects
	 */
	public function getProjects() {
		return $this->projects;
	}
	
	/**
	 * Return global options
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * Check if option is defined
	 * return boolean
	 */
	public function hasConfiguration($key) {
		return array_key_exists($key, $this->options);
	}

	/**
	 * Return option, or fallback if option is not defined
	 * return mixed
	 */
	public function getConfiguration($key, $fallback = null) {
		if ($this->hasConfiguration($key))
			return $this->options[$key];
		return $fallback;
	}

	/**
	 * Check if project is defined
	 * return boolean
	 */
	public function hasProject($key) {
		return array_key_exists($key, $this->projects);
	}

	/**
	 * Return project
	 * return Project
	 */
	public function getProject($key) {
		if ($this->hasProject($key))
			return $this->projects[$key];
		return null;
	}

	/*
	 * Load configuration file
	 */
	public function initialize() {
		$resolver = new OptionsResolver();
		$resolver->setDefaults(array(
			'auto' => false,
			'quiet' => false,
			'force' => false,
			'paths' => array(),
			'composer' => array(),
			'account' => array(),
        ));
		$resolver->setDefined(array(
			'os',
			'paths',
			'versioning',
			'framework',
			'composer',
			'account',
		));
		$resolver->setAllowedTypes('paths', 'array');
		$resolver->setAllowedTypes('composer', 'array');
		$resolver->setAllowedTypes('account', 'array');

		$resolver->setAllowedValues('os', array('linux', 'windows'));
		
		$resolver->setNormalizer('paths', function($options, $value) {
			$r = new OptionsResolver();
			$r->setDefined(array(
				'versioning',
				'php',
			));
			$r->setAllowedTypes('versioning', array('null', 'string'));
			$r->setAllowedTypes('php', array('null', 'string'));
			return $r->resolve($value);
		});
		$resolver->setNormalizer('composer', function($options, $value) {
			$r = new OptionsResolver();
			$r->setDefined(array(
				'install',
			));
			$r->setAllowedTypes('install', 'boolean');
			return $r->resolve($value);
		});
		$resolver->setNormalizer('account', function($options, $value) {
			$r = new OptionsResolver();
			$r->setDefined(array(
				'login',
				'password',
			));
			$r->setAllowedTypes('login', array('null', 'string'));
			$r->setAllowedTypes('password', array('null', 'string'));
			return $r->resolve($value);
		});
		// extract only global options (no projects) 
		// $options = $this->configYml;
		// unset($options['projects']);
		// $this->options = $resolver->resolve($options);
		$this->options = $resolver->resolve(array_diff_key($this->configYml, array('projects' => null)));
		$this->projects = array();
		foreach ($this->configYml['projects'] as $projectId => $project) {
			try {
				$this->projects[$projectId] = $this->projectConfiguration($project);
			}
			catch (\Exception $e) {
				throw new \Exception("{$projectId} : {$e->getMessage()}");
			}
		}
	}

	/*
	 * Load project configuration file
	 */
	protected function projectConfiguration($project) {
		$globals = $this->options;
		$resolver = new OptionsResolver();
		$resolver->setDefaults(array(
			'os' => $this->options['os'],
			'auto' => $this->options['auto'],
			'quiet' => $this->options['quiet'],
			'force' => $this->options['force'],
			'paths' => $this->options['paths'],
			'composer' => $this->options['composer'],
			'account' => $this->options['account'],
        ));
		$resolver->setRequired(array(
			'name',
			'paths',
		));
		$resolver->setDefined(array(
			'versioning',
			'framework',
			'composer',
			'account',
		));
		$resolver->setAllowedTypes('name', 'string');
		$resolver->setAllowedTypes('paths', 'array');
		$resolver->setAllowedTypes('versioning', array('null', 'array'));
		$resolver->setAllowedTypes('framework', array('null', 'array'));
		$resolver->setAllowedTypes('composer', array('null', 'array'));
		$resolver->setAllowedTypes('account', array('null', 'array'));
		
		$resolver->setNormalizer('paths', function($options, $value) use ($globals) {
			$r = new OptionsResolver();
			$r->setDefaults($globals['paths']);
			$r->setDefined(array(
				'versioning',
				'project',
				'repository',
				'php',
			));
			$r->setAllowedTypes('versioning', array('null', 'string'));
			$r->setAllowedTypes('project', array('null', 'string'));
			$r->setAllowedTypes('repository', array('null', 'string'));
			$r->setAllowedTypes('php', array('null', 'string'));
			return $r->resolve($value);
		});
		$resolver->setNormalizer('versioning', function($options, $value) {
			$r = new OptionsResolver();
			$r->setDefaults(array(
				'branch' => 'master',
			));
			$r->setRequired(array(
				'type',
				'branch',
			));
			$r->setAllowedTypes('type', array('null', 'string'));
			$r->setAllowedTypes('branch', array('null', 'string'));
			return $r->resolve($value);
		});
		$resolver->setNormalizer('framework', function($options, $value) {
			$r = new OptionsResolver();
			$r->setRequired(array(
				'name',
			));
			$r->setDefined(array(
				'environement',
				'options',
			));
			$r->setAllowedTypes('name', 'string');
			$r->setAllowedTypes('environement', array('null', 'string'));
			$r->setAllowedTypes('options', array('null', 'array'));
			return $r->resolve($value);
		});
		$resolver->setNormalizer('composer', function($options, $value) use ($globals) {
			$r = new OptionsResolver();
			$r->setDefaults($globals['composer']);
			$r->setRequired(array(
				'install',
			));
			$r->setAllowedTypes('install', 'boolean');
			return $r->resolve($value);
		});
		$resolver->setNormalizer('account', function($options, $value) use ($globals) {
			$r = new OptionsResolver();
			$r->setDefaults($globals['account']);
			$r->setDefined(array(
				'login',
				'password',
			));
			$r->setAllowedTypes('login', array('null', 'string'));
			$r->setAllowedTypes('password', array('null', 'string'));
			return $r->resolve($value);
		});
		
		if (isset($project['config']) && isset($project['steps'])) {
			$steps = array();
			foreach ($project['steps'] as $step => $active) {
				if (!$active || strtolower($active) == 'false') {
					continue;
				}
				$steps[] = array(strtolower($step) => $active);
			}
			return array(
				'options' => $resolver->resolve($project['config']),
				'steps' => $steps
			);
		}
		else if (!isset($project['config'])) {
			throw new \Exception("Configuration file is missing configuration label");
		}
		else if (!isset($project['steps'])) {
			throw new \Exception("No steps definition found");
		}
	}
}
