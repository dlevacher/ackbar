<?php

namespace Ackbar;

/**
 * Description of Composer
 *
 * @author BLONDEAU Gabriel
 * @author Levacher Damien
 */
class Composer {

	protected $pathPHP = "php";
	protected $pathProject = "path/to/your/project";
	protected $options = array();
	protected $exe;
	protected $os;

	public function __construct($pathPHP, $pathProject, $os, $install = false, array $options = array()) {
		if (!file_exists($pathProject)) {
			throw new RuntimeException("'{$pathProject}' is not a valid path for project");
		}
		if ($options && !is_array($options)) {
			throw new RuntimeException('Frameworks options expect an array, '.gettype($options).' given !');
		}
		$this->pathPHP = $pathPHP;
		$this->pathProject = $pathProject;
		$this->os = $os;
		$this->exe = $install === true ? 'composer' : $this->pathPHP . ' composer.phar';
	}

	protected function getActionOptions($action, $options = null) {
		return (isset($this->options[$action]) ? $this->options[$action] : "") . ' ' . $options;
	}

	public function selfupdate($options = null) {
		$output = array(
			'title' => 'selfUpdate',
			'action' => 'SelfUpdate'
		);

		try {
			$output['content'] = $this->run(sprintf("selfupdate --no-interaction %s", $this->getActionOptions($output['action'], $options)));
			$output['content'] = strlen($output['content']) == 0 ? 'You are already using composer version' : 'Update Ok.';
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;

			return $output;
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['content'] = $ex->getMessage();
			$output['code'] = $ex->getCode();

			return $output;
		}
	}

	public function update($options = null) {
		$output = array(
			'title' => 'update',
			'action' => 'update'
		);

		try {
			ini_set('max_execution_time', 0);
			$output['content'] = $this->run(sprintf("update --no-interaction %s", $this->getActionOptions($output['action'], $options)));
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;

			return $output;
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['content'] = $ex->getMessage();
			$output['code'] = $ex->getCode();

			return $output;
		}
	}

	public function run($cmd) {
		$cmd = preg_replace('/^composer\s/', '', $cmd);

		if (strtolower($this->os) == 'linux') {
			$commandToRun = sprintf('cd %s && COMPOSER_HOME="%s" %s', escapeshellarg($this->pathProject), sys_get_temp_dir(), $this->exe . ' ' . $cmd);
		} else {
			$commandToRun = sprintf('cd %s && %s', escapeshellarg($this->pathProject), $this->exe . ' ' . $cmd);
		}
		ob_start();
		passthru($commandToRun, $returnVar);
		$output = trim(ob_get_clean());

		if ($returnVar === 0) {
			//it's OK
		} else {
			throw new ComposerRunTimeException(
			sprintf(
					'Command %s failed with code : %s: %s', $commandToRun, $returnVar, $output
			), $returnVar);
		}

		return $output;
	}

}

Class ComposerRunTimeException extends \RuntimeException {

}
