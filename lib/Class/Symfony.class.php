<?php

namespace Ackbar;

/**
 * Description of Synfony
 *
 * @author Blondeau Gabriel
 * @author Levacher Damien
 */
class Symfony {

	protected $pathPHP = "php";
	protected $pathProject = "path/to/your/project";
	protected $environment = "prod";
	protected $options = array();
	protected $name = "symfony";
	protected $cmdOutput = null;

	public function __construct($pathPHP, $pathProject, $environment = null, array $options = array()) {
		if (!file_exists($pathProject)) {
			throw new RuntimeException("'{$pathProject}' is not a valid path for project");
		}
		if ($options && !is_array($options)) {
			throw new RuntimeException('Frameworks options expect an array, '.gettype($options).' given !');
		}
		$this->pathPHP = $pathPHP;
		$this->pathProject = $pathProject;
		if ($environment)	$this->environment = $environment;
		$this->options = array_merge($this->options, $options);
	}

	public function checkPhpIsFind() {
		if (!strtolower($this->pathPHP) == "php" && !file_exists($this->pathPHP)) {
			throw new InvalidPathPhpException($this->pathPHP . ' is not a valid path for php');
		}
	}

	public function getEnvironment() {
		return $this->environment;
	}

	protected function getActionOptions($action, $options = null) {
		return (isset($this->options[$action]) ? $this->options[$action] : "") . ' ' . $options;
	}

	public function maintenanceLock($options = null) {
		$action = 'maintenance:lock';
		$output = array(
			'title' => 'Maintenance lock',
			'action' => $action,
			'framework' => $this->name,
		);
		try {
			$output['content'] = $this->run(sprintf("app/console lexik:maintenance:lock -n %s", $this->getActionOptions($action, $options)));
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;
		} catch (Exception $ex) {
			$output['error'] = "Error on maintenance lock\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;
			$output['response'] = AckbarEvents::ACKRES_ERROR;
		}
		return $output;
	}

	public function maintenanceUnlock($options = null) {
		$action = 'maintenance:unlock';
		$output = array(
			'title' => 'Maintenance unlock',
			'action' => $action,
			'framework' => $this->name,
		);
		try {
			$output['content'] = $this->run(sprintf("app/console lexik:maintenance:unlock -n %s", $this->getActionOptions($action, $options)));
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;
		} catch (Exception $ex) {
			$output['error'] = "Error on maintenance unlock\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;
			$output['response'] = AckbarEvents::ACKRES_ERROR;
		}
		return $output;
	}

	public function clearCache($path, $options = null) {
		$action = 'clearCache';
		$output = array(
			'title' => 'Clear Cache',
			'action' => $action,
			'framework' => $this->name,
		);
		if (file_exists($path . '/app/config/config_' . $this->environment . '.yml')) {
			try {
				$output['content'] = $this->run(sprintf("app/console cache:clear --env=%s %s", $this->environment, $this->getActionOptions($action, $options)));
				$output['response'] = AckbarEvents::ACKRES_CONTINUE;
			} catch (Exception $ex) {
				$output['content'] = $this->cmdOutput;
				$output['error'] = $ex->getMessage();
				$output['response'] = AckbarEvents::ACKRES_END_ERROR;
			}
		} else {
			$output['error'] = "Error on clear cache";
			$output['response'] = AckbarEvents::ACKRES_END_ERROR;
			$output['content'] = "Environment : " . $this->environment . " not exist.";
			$output['code'] = 404;
		}
		return $output;
	}

	public function assets($options = null) {
		$action = 'assets';
		$output = array(
			'title' => 'Assets install',
			'action' => $action,
			'framework' => $this->name,
		);
		try {
			$output['content'] = $this->run(sprintf("app/console assets:install --env=%s %s", $this->environment, $this->getActionOptions($action, $options)));
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;
		} catch (Exception $ex) {
			$output['error'] = "Error on install bundle assets\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;
			$output['response'] = AckbarEvents::ACKRES_ERROR;
		}
		return $output;
	}

	public function asseticDump($options = null) {
		$action = 'asseticDump';
		$output = array(
			'title' => 'Assetic Dump',
			'action' => $action,
			'framework' => $this->name,
		);
		try {
			$output['content'] = $this->run(sprintf("app/console assetic:dump --env=%s %s", $this->environment, $this->getActionOptions($action, $options)));
			$output['response'] = AckbarEvents::ACKRES_CONTINUE;
		} catch (Exception $ex) {
			$output['error'] = "Error on dump assetic assets\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;
			$output['response'] = AckbarEvents::ACKRES_ERROR;
		}
		return $output;
	}

	public function fosJsRouting($options = null) {
		$action = 'fosJsRouting';
		$output = array(
			'title' => 'FriendsOfSymfony : JS Routing',
			'action' => $action,
			'framework' => $this->name,
			'response' => AckbarEvents::ACKRES_CONTINUE
		);

		try {
			$output['content'] = $this->run(sprintf("app/console fos:js-routing:dump --env=%s %s", $this->environment, $this->getActionOptions($action, $options)));

			return $output;
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['error'] = "Error on FriendsOfSymfony JsRouting dump\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;

			return $output;
		}
	}

	public function doctrineMigration($options = null) {
		$action = 'doctrineMigration';
		$output = array(
			'title' => 'Doctrine Migrations migrate',
			'action' => $action,
			'framework' => $this->name,
			'response' => AckbarEvents::ACKRES_CONTINUE
		);

		try {
			$output['content'] = $this->run(sprintf("app/console doctrine:migrations:migrate --allow-no-migration --no-interaction --env=%s %s", $this->environment, $this->getActionOptions($action, $options)));

			return $output;
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['error'] = "Error on Doctrine Migration\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;

			return $output;
		}
	}

	public function checkDbNeedUpdate() {
		$action = 'CheckDbNeedUpdate';
		$output = array(
			'title' => 'DB : need update ?',
			'action' => $action,
			'framework' => $this->name,
		);

		try {
			$output['content'] = $this->run(sprintf("app/console doctrine:schema:update --dump-sql %s", $this->getActionOptions($action)));

			$bool = (strpos($output['content'], "Nothing to update") !== false) ||
					(strpos($output['content'], "No Metadata Classes to process.") !== false) ? true : false;

			if ($bool) {
				$output['response'] = AckbarEvents::ACKRES_CONTINUE;
			} else {
				$output['additionalFunction'] = 'dbUpdate';
				$output['event'] = 'framework.dbupdate';
				$output['response'] = AckbarEvents::ACKRES_WAIT;
			}
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['error'] = "Error on check database state\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;
		}
		return $output;
	}

	public function dbUpdate() {
		$action = 'dbUpdate';
		$output = array(
			'title' => 'DB : update',
			'action' => $action,
			'framework' => $this->name,
			'response' => AckbarEvents::ACKRES_CONTINUE
		);

		try {
			$output['content'] = $this->run(sprintf("app/console doctrine:schema:update --force %s", $this->getActionOptions($action)));

			return $output;
		} catch (Exception $ex) {
			$output['response'] = AckbarEvents::ACKRES_ERROR;
			$output['error'] = "Error on update database\n".$ex->getMessage();
			$output['code'] = $ex->getCode();
			$output['content'] = $this->cmdOutput;

			return $output;
		}
	}

	public function run($cmd) {
		$cmd = preg_replace('/^php\s/', '', $cmd);
		$commandToRun = sprintf('cd %s && %s', escapeshellarg($this->pathProject), $this->pathPHP . ' ' . escapeshellcmd($cmd));
		ob_start();
		passthru($commandToRun, $returnVar);
		$this->cmdOutput = trim(ob_get_clean());

		if ($returnVar === 0) {
			//it's OK
		} else {
			throw new PhpRunTimeException(sprintf("Command %s failed with code [%s]\n %s", $commandToRun, $returnVar, $this->cmdOutput), $returnVar);
		}

		return $this->cmdOutput;
	}

}

class InvalidPathPhpException extends \InvalidArgumentException {

}

class PhpRunTimeException extends \RuntimeException {
	/*protected $output = null;

	public function __construct($output, $message) {
		parent::__construct($message);
		$this->output = $output;
	}

	public function getOutput() {
		return $this->output;
	}*/
}
