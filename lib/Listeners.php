<?php

namespace Ackbar;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of Listeners
 *
 * @author Blondeau Gabriel
 * @author Levacher Damien <damien.levacher@it-sis.com>
 */
class Listeners {

	public function onDefault(Event $event) {
		$result = array(
			'response' => AckbarEvents::ACKRES_CONTINUE,
			'content' => "",
			'title' => 'Default',
			'action' => 'Default action',
			'template' => $event->getTemplate(),
		);

		$event->setResponse($result);
	}

	public function onConfigurationStart(Event $event) {
		$result = array(
			'response' => AckbarEvents::ACKRES_WAIT,
			'content' => array("" => <<<XWING
                                               ____________
                                --)-----------|____________|
                                              ,'       ,'
                -)------========            ,'  ____ ,'
                         `.    `.         ,'  ,'__ ,'
                           `.    `.     ,'       ,'
                             `.    `._,'_______,'__
                               [._ _| ^--      || |
                       ____,...-----|__________ll_|\
      ,.,..-------"""""     "----'                 ||
  .-""  |=========================== ______________ |
   "-...l_______________________    |  |'      || |_]
                                [`-.|__________ll_|
                              ,'    ,' `.        `.
                            ,'    ,'     `.    ____`.
                -)---------========        `.  `.____`.
                                             `.        `.
                                               `.________`.
                               --)-------------|___________|
XWING
			),
			'title' => 'Do or do not. There is no try.',
			'action' => 'Start',
			'template' => $event->getTemplate(),
			'force' => $event->getForce(),
		);

		$event->setResponse($result);
	}

	public function onConfigurationDisplay(Event $event) {
		$output = $event->getProject()->getConfig();
		$result = array(
			'response' => $event->getProject()->getConfiguration('auto', false) ? AckbarEvents::ACKRES_CONTINUE : AckbarEvents::ACKRES_WAIT,
			'content' => $output,
			'title' => 'Configuration',
			'action' => 'Display',
			'template' => $event->getTemplate(),
			'force' => $event->getForce(),
		);

		$event->setResponse($result);
	}

	/* **************************************
	 * ************** VERSIONING ************
	 * ************************************** */

	public function onVersioningCheckFiles(Event $event) {
		$output = $event->getProject()->getVersioning()->checkFiles();
		$result = array(
			'response' => AckbarEvents::ACKRES_CONTINUE,
			'content' => $output['output'],
			'title' => 'Check Files',
			'next' => 'true',
			'additionalFunction' => 'updateClean',
			'event' => 'versioning.updateclean',
			'action' => 'CheckFiles',
			'template' => $event->getTemplate(),
			'type' => $event->getType(),
			'force' => $event->getForce()
		);
		if ($output['modified']) {
			$result['response'] = AckbarEvents::ACKRES_WAIT;
		}

		$event->setResponse($result);
	}

	public function onVersioningUpdateClean(Event $event) {
		$output = $event->getProject()->getVersioning()->updateClean();
		$result = array(
			'response' => AckbarEvents::ACKRES_CONTINUE,
			'content' => $output['output'],
			'title' => 'Sub step : Update Clean',
			'action' => 'UpdateClean',
			'template' => $event->getTemplate(),
			'type' => $event->getType()
		);
		$event->setResponse($result);
	}

	public function onVersioningPull(Event $event) {
		try {
			$output = $event->getProject()->getVersioning()->pull();
		} catch (Exception $ex) {
			$output = $ex;
		}

		$result = array(
			'response' => AckbarEvents::ACKRES_CONTINUE,
			'title' => 'Pulling',
			'action' => 'Pull',
			'template' => $event->getTemplate(),
			'alertStep' => '/!\\ If you use GIT, executing backup ignore local file modified /!\\',
			'type' => $event->getType(),
			'force' => $event->getForce()
		);

		if (is_object($output)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['content'] = strstr($output->getMessage(), "code 1:");
			$result['error'] = true;
			$result['additionalFunction'] = 'backup';
			$result['event'] = 'versioning.backup';
		} else {
			$result['content'] = $output['output'];
		}

		$event->setResponse($result);
	}

	public function onVersioningBackup(Event $event) {
		try {
			$output = $event->getProject()->getVersioning()->backupVersion($_SESSION['versioningVers']);
			$options = $event->getProject()->getOptions();
			$branch = $options['versioning']['branch'];
		} catch (Exception $ex) {
			$error = $ex;
		}

		$result = array(
			'response' => AckbarEvents::ACKRES_END_ERROR,
			'title' => 'Backup',
			'action' => 'BackupVersion',
			'template' => $event->getTemplate(),
			'type' => $event->getType()
		);

		if (isset($error)) {
			$result['content'] = $error->getMessage();
			$result['error'] = true;
		}else {
			$result['content'] =$output['output'];
		}

		$event->setResponse($result);
	}

	/* *************************************
	 * ************ FRAMEWORK **************
	 * ************************************* */

	public function onFrameworkMaintenanceLock(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->maintenanceLock($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkMaintenanceUnlock(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->maintenanceUnlock($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkClearCache(Event $event) {
		$options = $event->getProject()->getOptions();
		$path = $options['paths']['project'];
		try {
			$output = $event->getProject()->getFramework()->clearCache($path, $event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkAssets(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->assets($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkAsseticDump(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->asseticDump($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkFosJsRouting(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->fosJsRouting($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkDoctrineMigration(Event $event) {

		try {
			$output = $event->getProject()->getFramework()->doctrineMigration($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkCheckDbNeedUpdate(Event $event) {
		try {
			$output = $event->getProject()->getFramework()->checkDbNeedUpdate();
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
			$result['force'] = $event->getForce();
		}
		$result['template'] = $event->getTemplate();

		$event->setResponse($result);
	}

	public function onFrameworkDbUpdate(Event $event) {
		try {
			$output = $event->getProject()->getFramework()->dbUpdate();
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)) {
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		} else {
			$result = $output;
		}
		$result['template'] = $event->getTemplate();
		$event->setResponse($result);
	}

	/* **************************************
	 * ************ COMPOSER ****************
	 * ************************************** */
	public function onComposerSelfUpdate(Event $event) {
		try{
			$output = $event->getProject()->getComposer()->selfUpdate($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)){
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		}  else {
			$result = $output;
		}

		$result['template'] = $event->getTemplate();
		$event->setResponse($result);
	}

	 public function onComposerUpdate(Event $event) {
		if (Controller::getInterfaceType() == 'interface') echo '<script>composerUpdate("open");</script>';
		try{
			$output = $event->getProject()->getComposer()->Update($event->getParams());
		} catch (Exception $ex) {
			$error = $ex;
		}

		if (isset($error)){
			$result['response'] = AckbarEvents::ACKRES_ERROR;
			$result['error'] = $error;
		}  else {
			$result = $output;
		}
		if (Controller::getInterfaceType() == 'interface') echo '<script>composerUpdate("close");</script>';
		$result['template'] = $event->getTemplate();
		$event->setResponse($result);
	}
}
