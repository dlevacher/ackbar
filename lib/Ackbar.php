<?php

namespace Ackbar;

require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/Configuration.php';
require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/Project.php';

// class Ackbar implements \Serializable {
class Ackbar {
	protected $error_handler = null;
	protected $exception_handler = null;
	protected $destroy = false; // Interface initialized
	protected $currentProject = null; // current project id
	//protected $unserializedProject = null; // unserialized project not yet initialized
	protected $projects = array(); // projects
	protected $configuration = null; //Object Configuration
	protected $controller = null; //Object Controller

	public function __construct() {
		$this->initialize();
	}
	
	public function __destruct() {
		if (!$this->destroy)
			$this->Save();
	}
	
    public function __sleep()
    {
        return array('currentProject', 'controller');
    }

    public function __wakeup()
    {
		$this->initialize();
    }

	/*
	 * Initialize
	 * Initialize all object useful for the operation
	 * return object ackbar
	 */
	public function initialize() {
		$this->error_handler = set_error_handler(array($this, "error"));
		$this->exception_handler = set_exception_handler(array($this, "exception"));
		
		if ($opts = getopt("v::", array("version::"))) {
			foreach ($opts as $opt => $val) {
				switch ($opt) {
					case "v":
					case "version":
						echo file_get_contents(__DIR__.'/../Credit.txt');
						exit(0);
				}
			}
		}
		
		// Load configuration
		$this->configuration = new Configuration();
		$this->configuration->initialize();

		foreach ($this->configuration->getProjects() as $projectId => $config) {
			$this->projects[$projectId] = new Project($projectId, $config);
		}
		if (count($this->projects) == 0) {
			throw new \Exception("No projects defined");
		}
		else if (count($this->projects) == 1 && !($this->currentProject)) {
			$this->currentProject = current($this->projects);
		}
		global $argv;
		if (count($argv) > 1) {
			foreach ($argv as $i => $arg) {
				if ($i == 0) continue; // 1st if script name
				if (strpos($arg, '-') === 0) continue; // option - handle in configuration
				else if (array_key_exists($arg, $this->projects)) $this->currentProject = $this->projects[$arg];
				else throw new \Exception("Unknown project '{$arg}'");
			}
		}
		
		//Initialize Controller
		if (!$this->controller)
			$this->controller = new Controller($this->configuration->getOptions());
		$this->controller->initialize();
		
		if ($this->currentProject)
			$this->currentProject->initialize($this->controller);
		else
			$this->controller->viewInterface($this->projects);
		
		return $this;
	}

	/**
	 * Get interface type
	 */
	public function getInterfaceType() {
		return $this->controller ? $this->controller->getType() : Controller::getInterfaceType();
	}

	/**
	 * Check if configuration key exists
	 */
	public function hasConfiguration($key) {
		return $this->configuration ? $this->configuration->hasConfiguration($key, $parent) : false;
	}

	/**
	 * Retrun configuration key
	 */
	public function getConfiguration($key, $fallback = null) {
		return $this->configuration ? $this->configuration->getConfiguration($key, $fallback) : $fallback;
	}

	/*
	 * Project
	 * Change project
	 */
	public function Project($projectId) {
		if ($this->currentProject)
			$this->currentProject->endStep();
		$this->currentProject = null;
		if (array_key_exists($projectId, $this->projects)) {
			$this->currentProject = $this->projects[$projectId];
			$this->currentProject->initialize($this->controller);
		}
		return $this;
	}

	/*
	 * Run
	 * Execute step by step
	 */
	public function Run() {
		if ($this->currentProject)
			$this->currentProject->Run();
		return $this;
	}

	/*
	 * Next
	 * Add +1 on currentStep, change index on table
	 */

	public function Next($jump = 1) {
		if ($this->currentProject)
			$this->currentProject->Next($jump);
		return $this;
	}

	/*
	 * endStep
	 * Reset all SESSION variable when Ackbar finish to work
	 * return Ackbar
	 */
	public function endStep() {
		if ($this->currentProject)
			$this->currentProject->endStep();
		$this->currentProject = null;
		return $this;
	}

	/*
	 * restartAckbar
	 * Reset all SESSION variable
	 * And Refresh page
	 */
	public function Restart() {
		unset($_SESSION['Ackbar']);
		session_destroy();
		if ($this->currentProject) $this->currentProject->destroy();
		$this->destroy = true;
		return $this;
	}
	
	/**
	 * Save Ackbar to session
	 */
	public function Save() {
		$_SESSION['Ackbar'] = serialize($this);
		return $this;
	}
	
	/**
	 * Load Ackbar from session (or create new one)
	 */
	public static function getInstance() {
		if (isset($_SESSION['Ackbar'])) {
			return unserialize($_SESSION['Ackbar']);
		}
		return new Ackbar();
	}

	/*
	 * getController
	 * return object Controller
	 */
	public function getController() {
		return $this->controller;
	}
	
	public function error($errno, $error, $errfile, $errline) {
	   if (!(error_reporting() & $errno)) {
			// Ce code d'erreur n'est pas inclus dans error_reporting()
			return ;
		}
		if ($this->controller) {
			return $this->controller->error($errno, $error, $errfile, $errline);
		}
		
		$msg  = "--------------------------------------------------------------------------------\n";
		$msg .= "-                            /!\ ERROR /!\                                     -\n";
		$msg .= "--------------------------------------------------------------------------------\n";
		$msg .= "[{$errno}] {$error}\n";
		$msg .= "{$errfile}:{$errline}\n";
		echo (php_sapi_name() == 'cli') ? $msg : nl2br($msg);
		return false;
	}
	
	public function exception($exception) {
		if ($this->controller) {
			return $this->controller->exception($exception);
		}
		$msg  = "--------------------------------------------------------------------------------\n";
		$msg .= "-                            /!\ ERROR /!\                                     -\n";
		$msg .= "--------------------------------------------------------------------------------\n";
		$msg .= "{$exception->getMessage()}\n";
		echo (php_sapi_name() == 'cli') ? $msg : nl2br($msg);
	}

}
