<?php

namespace Ackbar;

/*
 * EVENTS
 */
require_once __DIR__ . '/Events/TypeEvent.php';
//VERSIONING
require_once __DIR__ . '/Events/Versioning/VersioningDefaultEvent.php';
require_once __DIR__ . '/Events/Versioning/VersioningForceEvent.php';


/**
 * Description of AckbarEvents
 *
 * @author user
 */
final class AckbarEvents {
	//STEPS
	const ACKRES_CONTINUE = "next";
	const ACKRES_WAIT = "wait";
	const ACKRES_STOP = "stop";
	const ACKRES_ERROR = "error";
	const ACKRES_END = "finish";
	const ACKRES_END_ERROR = "enderror";

	public static $events = array(
		'configuration.start' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_CONFIGURATION,
			'listener' => 'onConfigurationStart'
		),
		'configuration.display' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_CONFIGURATION,
			'listener' => 'onConfigurationDisplay'
		),
		/*
		 * Versioning
		 */
		'versioning.checkfiles' => array(
			'className' => "VersioningForceEvent",
			'template' => TypeEvent::EVENT_VERSIONING,
			'listener' => 'onVersioningCheckFiles',
		),
		'versioning.updateclean' => array(
			'className' => "VersioningDefaultEvent",
			'template' => TypeEvent::EVENT_VERSIONING,
			'listener' => 'onVersioningUpdateClean',
		),
		'versioning.pull' => array(
			'className' => "VersioningForceEvent",
			'template' => TypeEvent::EVENT_VERSIONING,
			'listener' => 'onVersioningPull',
		),
		'versioning.backup' => array(
			'className' => "VersioningDefaultEvent",
			'template' => TypeEvent::EVENT_VERSIONING,
			'listener' => 'onVersioningBackup',
		),
		/*
		 * Framework
		 */
		'framework.maintenancelock' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkMaintenanceLock',
		),
		'framework.maintenanceunlock' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkMaintenanceUnlock',
		),
		'framework.clearcache' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkClearCache',
		),
		'framework.assets' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkAssets',
		),
		'framework.asseticdump' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkAsseticDump',
		),
		'framework.fosjsrouting' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkFosJsRouting',
		),
		'framework.doctrinemigration' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkDoctrineMigration',
		),
		'framework.checkdbneedupdate' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkCheckDbNeedUpdate',
		),
		'framework.dbupdate' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_FRAMEWORK,
			'listener' => 'onFrameworkDbUpdate',
		),
		/*
		 * Composer
		 */
		'composer.selfupdate' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_COMPOSER,
			'listener' => 'onComposerSelfUpdate',
		),
		'composer.update' => array(
			'className' => "TypeEvent",
			'template' => TypeEvent::EVENT_COMPOSER,
			'listener' => 'onComposerUpdate',
		),
	);

}
