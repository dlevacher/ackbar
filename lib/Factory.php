<?php

namespace Ackbar;

/**
 * Description of Factory
 *
 * @author Levacher Damien <damien.levacher@it-sis.com>
 */
class Factory {
	/*
	 * * CreateVers
	 */

	public static function createVers($configuration) {
		if (!isset($configuration['versioning']['type']))
			throw new \RuntimeException('Versioning type not defined !');
		if (!isset($configuration['paths']['project']))
			throw new \RuntimeException('Project path not defined !');
		$vers = ucfirst($configuration['versioning']['type']);
		$namespace = 'PHP' . $vers;
		$class = "${namespace}\\Repository";

		try {
			if (!class_exists($class)) {
				throw new \RuntimeException('Versioning "' . $vers . '" not found !');
			}
			return new $class($configuration['paths']['project'], false, array(
				$configuration['versioning']['type'] . '_executable' => ($configuration['paths']['versioning']) ? $configuration['paths']['versioning'] : $configuration['versioning']['type'],
				'login' => isset($configuration['account']) && isset($configuration['account']['login']) ? $configuration['account']['login'] : null,
				'password' => isset($configuration['account']) && isset($configuration['account']['password']) ? $configuration['account']['password'] : null,
				'repository' => isset($configuration['paths']['repository']) ? $configuration['paths']['repository'] : null
			));
		} catch (RuntimeException $e) {
			echo $e->getMessage();
		}
	}

	/*
	 * * CreateFramework
	 */

	public static function createFramework($configuration) {

		if (!isset($configuration['framework']['name']))
			throw new \RuntimeException('Framework name not defined !');
		if (!isset($configuration['paths']['project']))
			throw new \RuntimeException('Project path not defined !');
		$framework = ucfirst($configuration['framework']['name']);
		$path = __DIR__ . "/Class/";
		try {
			if (!file_exists($path = $path . $framework . '.class.php')) {
				throw new \RuntimeException('Framework "' . $framework . '" not found !');
			}
			require_once $path;
			$framework = "Ackbar\\".$framework;
			return new $framework(
					isset($configuration['paths']['php']) ? $configuration['paths']['php'] : "php",
					$configuration['paths']['project'],
					isset($configuration['framework']['environement']) ? $configuration['framework']['environement'] : null,
					isset($configuration['framework']['options']) ? $configuration['framework']['options'] : array()
					);
		} catch (RuntimeException $e) {
			echo $e->getMessage();
		}
	}
	
	/*
	 ** CreateComposer
	 */
	public static function CreateComposer($configuration)
	{
		if (!isset($configuration['paths']['project']))
			throw new \RuntimeException('Project path not defined !');
		if (!isset($configuration['os']))
			throw new \RuntimeException('OS not defined !');
		$composer = ucfirst("Composer");
		$path = __DIR__ . "/Class/";
		try {
			if (!file_exists($path = $path . $composer . '.class.php')) {
				throw new \RuntimeException('Composer "' . $composer . '" not found !');
			}
			require_once $path;
			$composer = "Ackbar\\".$composer;
			return new $composer(
					isset($configuration['paths']['php']) ? $configuration['paths']['php'] : "php",
					$configuration['paths']['project'],
					$configuration['os'],
					isset($configuration['composer']['install']) ? $configuration['composer']['install'] : false,
					isset($configuration['composer']['options']) ? $configuration['composer']['options'] : array()
					);
		} catch (RuntimeException $e) {
			echo $e->getMessage();
		}
	}
}
