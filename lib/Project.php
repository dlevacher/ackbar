<?php

namespace Ackbar;

// require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/Listeners.php';
require_once __DIR__ . '/Events.php';
require_once __DIR__ . '/Factory.php';
require_once __DIR__ . '/Class/Composer.class.php';

use Symfony\Component\EventDispatcher\EventDispatcher;
use Ackbar\Factory;

/**
 * Description of AckbarProject
 *
 * @author Damien Levacher https://github.com/dlevacher
 */

class Project {
	protected $id = null;
	protected $name = "";
	protected $currentStep = 0;
	protected $versioningVers = null;
	protected $steps = array();
	protected $options = array();
	protected $initialized = false;
	protected $interface = false; // Interface initialized
	protected $versioning = null; //Object Repository
	protected $framework = null; //Object Framework
	protected $composer = null; //Object Composer
	protected $eventLists = array(); //List of Events
	protected $dispatcher = null; //Object dispatcher
	protected $listener = null; //Object listener
	protected $controller = null; //Object Controller
	
	public function __construct($id, $config) {
		$this->id = $id;
		$this->name = isset($config['options']['name']) ? $config['options']['name'] : null;
		$this->options = $config['options'];
		$this->steps = $config['steps'];
	}
	
	/**
	 * return Id
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * return project name
	 */
	public function getName() {
		return $this->name;
	}
	
    public function __sleep()
    {
        return array('id', 'name', 'options', 'steps', 'currentStep', 'versioningVers');
    }

    public function __wakeup()
    {
    }
	
	/*
	 * Initialize
	 * Initialize all object useful for the operation
	 * return object ackbar
	 */
	public function initialize(Controller $controller) {
		if (!$this->initialized) {
			$this->initialized = true;
			$this->controller = $controller;
			$this->name = $this->getConfiguration('name');
			
			$this->dispatcher = new EventDispatcher();
			$this->listener = new Listeners();
			/*
			 * Add events according step
			 */
			foreach ($this->steps as $step) {
				try {
					$this->addEvent(key($step), $step[key($step)]);
				} catch (Exception $e) {
					throw new \Exception("Pas content");
				}
			}
			
			/*
			 * Create objects if they are in configuration
			 */
			//Object Versioning
			if ($this->hasConfiguration('versioning')) {
				$this->versioning = Factory::createVers($this->options);
				if (!$this->versioningVers) $this->versioningVers = $this->versioning->checkVers();
			}
			//Object Framework
			if ($this->hasConfiguration('framework')) {
				$this->framework = Factory::createFramework($this->options);
			}
			//Object Composer
			if ($this->hasConfiguration('composer')) {
				$this->composer = Factory::createComposer($this->options);
			}
			
			$this->controller->setName($this->getName())->viewInterface(); 
		}

		return $this;
	}
	
	/**
	 *
	 */
	public function destroy() {
		if ($this->versioning) $this->versioning->clearConfiguration();
	}

	/**
	 * Check if configuration key exists
	 */
	public function hasConfiguration($key) {
		return array_key_exists($key, $this->options);
	}

	/**
	 * Retrun configuration key
	 */
	public function getConfiguration($key, $fallback = null) {
		if ($this->hasConfiguration($key))
			return $this->options[$key];
		return $fallback;
	}

	/**
	 * getNbSteps
	 * return count of steps
	 */

	public function getNbSteps() {
		return count($this->steps);
	}

	/**
	 * getTypeVersionning
	 * return string versioning type (Hg / git)
	 */
	public function getTypeVersionning() {
		return isset($this->options['versioning']['type']) ? $this->options['versioning']['type'] : null;
	}

	/**
	 * getOptions
	 * return array options
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * getForce
	 * return boolean option 'force'
	 */
	public function getForce() {
		return $this->getConfiguration('force', false);
	}

	/**
	 * getFramework
	 * return object Framework
	 */
	public function getFramework() {
		return $this->framework;
	}

	/**
	 * getComposer
	 * return object Composer
	 */
	public function getComposer() {
		return $this->composer;
	}

	/*
	 * getController
	 * return object Controller
	 */
	public function getController() {
		return $this->controller;
	}

	/**
	 * getVersioning
	 * return object Repository
	 */
	public function getVersioning() {
		return $this->versioning;
	}

	/*
	 * getConfig
	 * Give essential information about project config
	 * Name project / type (interface, quiet etc... / Framework / Versioning / Composer / etc...
	 */
	public function getConfig() {
		$i = 0;
		$config = array();
		$config['name'] = $this->getConfiguration('name');
		$config['auto'] = $this->getConfiguration('auto', false);
		$config['quiet'] = $this->getConfiguration('quiet', false);
		$config['force'] = $this->getConfiguration('force', false);
		$config['paths'] = $this->getConfiguration('paths');
		if (isset($this->versioning)) {
			$config['versioning'] = $this->options['versioning'];
			$config['logos'][$i] = $this->getTypeVersionning();
			$i++;
		}
		if (isset($this->framework)) {
			$config['framework'] = $this->options['framework'];
			$config['logos'][$i] = $this->options['framework']['name'];
			$i++;
		}
		if (isset($this->composer)) {
			$config['composer'] = $this->options['composer'];
			$config['logos'][$i] = 'composer';
			$i++;
		}
		
		$config['steps'] = array();
		foreach ($this->steps as $key => $step)
			$config['steps'][key($step)] = $step[key($step)];

		return $config;
	}

	/*
	 * Run
	 * Execute step by step
	 */
	public function Run() {
		if ($this->currentStep == 0 && !$this->getConfiguration('auto', false) && !$this->getConfiguration('force', false) && !$this->getConfiguration('quiet', false)) {
			if (key($this->steps[$this->currentStep]) != 'configuration.display') {
				$this->introduceStep('configuration.start', 0);
			}
		}
		while ($this->currentStep <= $this->getNbSteps()) {
			$step = key($this->steps[$this->currentStep]);

			$event = $this->dispatcher->dispatch($step, $this->eventLists[$step]);
			$result = $event->getResponse();
			if ((($this->currentStep + 1) == $this->getNbSteps()) && ($result['response'] != AckbarEvents::ACKRES_WAIT) && ($result['response'] != AckbarEvents::ACKRES_ERROR)) {
				$result['response'] = AckbarEvents::ACKRES_END;
			}

			if (!$this->options['quiet']) {
				$jump = 1;
				if ($result['response'] == AckbarEvents::ACKRES_END_ERROR) {
					$this->controller->output($result);
					$this->endStep();
					break;
				}

				if ((($this->currentStep + 1) == $this->getNbSteps()) && ($result['response'] == AckbarEvents::ACKRES_WAIT)) {
					$result['next'] = false;
				}
				$this->getController()->output($result);

				if ($result['response'] == AckbarEvents::ACKRES_END) {
					$this->endStep();
					break;
				}

				if ($result['response'] == AckbarEvents::ACKRES_WAIT) {
					if (isset($result['event'])) $this->introduceStep($result['event']);
					if (!$this->options['force']) {
						if (($jump = $this->getController()->input($this)) === null)
							break;
					}
				}

				if ($result['response'] == AckbarEvents::ACKRES_ERROR) {
					if (isset($result['event'])) {
						$this->introduceStep($result['event']);
					}
					if (($jump = $this->getController()->input($this)) === null)
						break;
				}
				$this->Next($jump);
			} else {
				if ($result['response'] == AckbarEvents::ACKRES_CONTINUE) {
					$this->controller->output($result);
					$this->Next();
				} else if ($result['response'] == AckbarEvents::ACKRES_WAIT && $this->options['force']) {
					$this->introduceStep($result['event']);
					$this->Next();
				} else if ($result['response'] == AckbarEvents::ACKRES_END) {
					$this->controller->output($result);
					$this->endStep();
					break;
				} else {
					$result['stop'] = true;
					$this->controller->output($result);
					$this->endStep();
					break;
				}
			}
		}
	}

	/*
	 * Next
	 * Add +1 on currentStep, change index on table
	 */
	public function Next($jump = 1) {
		if ($this->currentStep + $jump > $this->getNbSteps())
			throw new \Exception("Jumping too high : No more steps !!");
		$this->currentStep += $jump;
		return $this;
	}

	/*
	 * endStep
	 * Reset all SESSION variable when Ackbar finish to work
	 * return AckbarProject
	 */
	public function endStep() {
		$this->currentStep = 0;
		$this->steps = array();
		
		return $this;
	}

	/*
	 * addEvent
	 * Add one event in $eventLists and Add listener in $dispatcher
	 */
	public function addEvent($step, $params = null) {
		try {
			$event = AckbarEvents::$events[$step];
			$this->dispatcher->addListener($step, array($this->listener, isset($event['listener']) ? $event['listener'] : "onDefault"));
			
			$class = "Ackbar\\".$event['className'];
			$template = isset($event['template']) ? $event['template'] : "default";
			$this->eventLists[$step] = new $class($this, $template, $params !== true ? $params : null);
		} catch (Exception $e) {
			throw new \Exception("Pas content : " . $e->getMessage());
		}
	}

	/*
	 * introduceStep
	 * Add step in $this->steps
	 * call addEvent to add event in $this->eventLists
	 */
	public function introduceStep($step, $pos = null) {
		$this->steps = array_merge(array_slice($this->steps, 0, (is_null($pos) ? $this->currentStep + 1 : $pos)), array(array($step=> true)), array_slice($this->steps, (is_null($pos) ? $this->currentStep + 1 : $pos)));
		$this->addEvent($step);
	}
}