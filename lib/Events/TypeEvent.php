<?php

namespace Ackbar;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of TypeEvent
 *
 * @author BLONDEAU Gabriel
 */
class TypeEvent extends Event {

	protected $project;
	protected $response = null;
	protected $template;
	protected $params = null;

	const EVENT_VERSIONING = 'step';
	const EVENT_FRAMEWORK = 'frameworkStep';
	const EVENT_COMPOSER = 'composer';
	const EVENT_CONFIGURATION = 'config';

	public function __construct(Project $project, $template, $params = null) {
		$this->project = $project;
		$this->template = $template;
		$this->params = $params;
	}

	public function getProject() {
		return $this->project;
	}

	public function getResponse() {
		return $this->response;
	}

	public function setResponse($response) {
		$this->response = $response;
		return $this;
	}

	public function getTemplate() {
		return $this->template;
	}

	public function getParams() {
		return $this->params;
	}

	public function getForce() {
		return $this->project->getForce();
	}

	public function getType() {
		return $this->type;
	}

}
