<?php

namespace Ackbar;

require_once __DIR__ . '/../TypeEvent.php';

/**
 * Description of VersioningDefaultEvent
 *
 * @author Blondeau Gabriel
 */
class VersioningDefaultEvent extends TypeEvent {

	public function __construct(Project $project, $template, $params = null) {
		parent::__construct($project, $template, $params);
		$this->type = $project->getTypeVersionning();
	}

}
