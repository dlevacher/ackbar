<?php

namespace Ackbar;

// require_once __DIR__ . '/vendor/autoload.php';

// use Twig;
/**
 * Description of AckbarController
 *
 * @author user
 */
class Controller {
	private static $accepted_types = array(
		'interface',
		'cli',
	);
	protected $type = null;
	protected $name = "Ackbar";
	protected $ext = "txt";
	protected $interface = false;
	protected $options = array(
		'auto'	=> false,
		'quiet'	=> false,
	);
	// interface specific
	protected $twigLoader = null;
	protected $twig = null;
	
    public function __sleep()
    {
        return array('type', 'name', 'interface', 'options');
    }

    public function __wakeup()
    {
		$this->initialize();
    }
	
	public static function getInterfaceType() {
		if (php_sapi_name() == 'cli') 
			return 'cli';
		return 'interface';
	}

	public function __construct(array $options = array()) {
		/*if (!in_array($type, self::$accepted_types))
			throw new \Exception("Ackbar type '{$type}' unknown. Accepted types are ".implode(', ', self::$accepted_types));
		*/
		$this->type = self::getInterfaceType();
		$this->options = array_merge($this->options, array_intersect_key($options, $this->options));
	}
	
	public function initialize() {
		switch ($this->type) {
			case 'interface':
				$this->ext = "html";
			case 'cli':
				$this->twigLoader = new \Twig_Loader_Filesystem(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templatesTwig');
				$this->twig = new \Twig_Environment($this->twigLoader);
				break;
			default:
				throw new \Exception("Ackbar type '{$type}' unknown. Accepted types are ".implode(', ', self::$accepted_types));
		}
		return $this;
	}
	
	public function getType() {
		return $this->type;
	}
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function viewInterface($projects = null) {
		switch ($this->type) {
			case 'interface':
			case 'cli':
				if ($this->interface != true)
					echo $this->twig->render("index.{$this->ext}.twig", array(
						'name' => $this->name,
						'options' => $this->getOptions(),
						'projects' => $projects,
					));
				$this->interface = true;
				break;
			default:
				throw new \Exception("Ackbar type '{$type}' unknown. Accepted types are ".implode(', ', self::$accepted_types));
		}
	}
	
	public function getOptions() {
		return $this->options;
	}
	
	public function getOption($option, $fallback = null) {
		return (array_key_exists($option, $this->options) ? $this->options[$option] : $fallback);
	}

	public function output(array $outputListener) {
		switch ($this->type) {
			case 'interface':
			case 'cli':
				ob_start();
				if (!$this->getOption('quiet', false)) {
					echo $this->twig->render("{$outputListener['template']}.{$this->ext}.twig", $outputListener);
				} else if (($outputListener['response'] == AckbarEvents::ACKRES_END)
						|| ($outputListener['response'] == AckbarEvents::ACKRES_END_ERROR)
						|| ($outputListener['response'] == AckbarEvents::ACKRES_WAIT)) {
					echo $this->twig->render("finish.{$this->ext}.twig", $outputListener);
				}
				ob_end_flush();
				break;
			default:
				throw new \Exception("Ackbar type '{$type}' unknown. Accepted types are ".implode(', ', $this->accepted_types));
		}
	}

	public function input(Project $project = null) {
		switch ($this->type) {
			case 'interface':
				break;
			case 'cli':
				$line = trim(fgets(STDIN));
				if (strlen($line) == 0) // Next
					return 1;
				else if (strtolower($line[0]) == 's')
					return 2;
				else
					return $this->input($project);
				break;
			default:
				throw new \Exception("Ackbar type '{$type}' unknown. Accepted types are ".implode(', ', $this->accepted_types));
		}
		return null;
	}
	
	public function error($errno, $error, $errfile, $errline) {
		try {
			echo $this->twig->render("error.{$this->ext}.twig", array('errno' => $errno, 'error' => $error, 'errfile' => $errfile, 'errline' => $errline));
		}
		catch (\Exception $e) {
			$msg  = "--------------------------------------------------------------------------------\n";
			$msg .= "-                            /!\ ERROR /!\                                     -\n";
			$msg .= "--------------------------------------------------------------------------------\n";
			$msg .= "[{$errno}] {$error}\n";
			$msg .= "{$errfile}:{$errline}\n";
			echo (php_sapi_name() == 'cli') ? $msg : nl2br($msg);
		}
		switch ($errno) {
			case E_USER_ERROR:
				exit(1);
				break;
			case E_USER_WARNING:
			case E_USER_NOTICE:
			default:
				break;
		}
		return true;
	}
	
	public function exception($exception) {
		try {
			echo $this->twig->render("exception.{$this->ext}.twig", array('exception' => $exception->getMessage()));
		}
		catch (\Exception $e) {
			$msg  = "--------------------------------------------------------------------------------\n";
			$msg .= "-                            /!\ ERROR /!\                                     -\n";
			$msg .= "--------------------------------------------------------------------------------\n";
			$msg .= "{$exception->getMessage()}\n";
			echo (php_sapi_name() == 'cli') ? $msg : nl2br($msg);
		}
	}

}
