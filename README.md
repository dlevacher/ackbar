# it-sis/ackbar 

Update projects within the Alliance

It takes your project's `ackbar.yml` and execute the tasks
for your project update.

> Note: This project is for it-sis internal use ! It's been tested against our projects only.
Feel free to report any issues you encounter.

## Usage

### ackbar.phar

If multiple projects are available, the list will be displayed.

```bash
$ ackbar
```

### ackbar.phar [project]

The specified project update will be launched.

```bash
$ php ackbar.phar project
```

## Install

You can grab a copy of it-sis/ackbar in either of the following ways.

### As a phar (recommended)

You can simply download a pre-compiled and ready-to-use version as a Phar
to any directory:

```bash
$ wget http://damien.levacher.free.fr/ackbar/ackbar.phar
```

That's it. You can now verify everything works by running:

```bash
$ php ackbar.phar --version
```

#### Updating phar

There's no separate `update` procedure, simply overwrite the existing phar with the new version downloaded.

### Manual Installation from Source

This project requires PHP 5.3+ and clue/phar-composer:

```bash
$ hg clone https://dlevacher@bitbucket.org/dlevacher/ackbar
$ wget http://www.lueck.tv/phar-composer/phar-composer.phar
$ php phar-composer build ackbar
```

You can now verify everything works by running:

```bash
$ php ackbar.phar --version
```

#### Updating manually

```bash
$ hg pull
```

## Contributing
1. Fork it!
2. Create your feature branch: `hg branch my-new-feature`
3. Commit your changes: `hg commit -m 'Add some feature'`
4. Push to the branch: `hg push`
5. Submit a pull request :D

## License

MIT

## Credits

IT'S A TRAP (<https://www.youtube.com/watch?v=4F4qzPbcFiA>)

Contributors:

- Gabriel Blondeau
- Damien Levacher
- Admiral Ackbar (<http://en.wikipedia.org/wiki/Admiral_Ackbar>)

Thanks to Composer(<https://github.com/composer/composer/>), Phar-composer(<https://github.com/clue/phar-composer>) and Symfony(<http://symfony.com//>)